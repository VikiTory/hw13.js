/*---Теоретичне питання---
1) Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval()`.
Ключова відмінність між `setTimeout()`і `setInterval()` в тому, що перший виконує 
ф-цію лише один раз, а другий продовжує виконувати ф-цію через визначений інтервал,
доки не буде зупинено.
2) Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює 
вона миттєво і чому? 
Якщо передати нульову затримку ф-ції setTimeout(), ф-ція все одно не запуститься 
миттєво. Натомість його буде заплановано якнайшвидше, але перед його запуском все 
одно буде невелика затримка. Це пояснюється тим, що нульова затримка, передана 
setTimeout(), стосується min часу, протягом якого JS повинен чекати перед виконанням
ф-ції, а не фактичної нульової затримки. Коли передаємо нуль як затримку, JS поміщає
ф-цію в кінець черги виконання,що означає, що будь-який ін. код, який раніше був у 
черзі, виконуватиметься першим.
3) Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений 
цикл запуску вам вже не потрібен?
Важливо викликати функцію clearInterval(), щоб запобігти витоку пям'яті та ін. небажаній
поведінці(такі як безкінечної роботи інтервалу, затримка або повільна продуктивність  та ін.).


---Практичне завдання---*/

let cycleInterval;
let currentImgIndex = 0;
const images = ["./img/1.jpg", "./img/2.jpg", "./img/3.jpg", "./img/4.jpg"];

function startCycle() {
    cycleInterval = setInterval(() => {
        const imgUrl = images[currentImgIndex];
        const imgElement = document.getElementById("img");
        imgElement.src = imgUrl;
        currentImgIndex = (currentImgIndex + 1) % images.length;
        console.log(currentImgIndex);
        }, 3000);
        
}

window.onload = () => {
    startCycle();
}


function stopCycle() {
    clearInterval(cycleInterval);
    cycleInterval = null;
}

function resumeCycle() {
    if(!cycleInterval) {
        startCycle();
    }
}

window.onload = () => {
    startCycle();
    const stopButton = document.getElementById("stop");
    const resumeButton = document.getElementById("resume");
    stopButton.onclick = stopCycle;
    resumeButton.onclick = resumeCycle;
}